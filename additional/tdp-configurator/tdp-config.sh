#!/usr/local/bin/bash

RENDERED_FILES_DIR=../../rendered-files

DEV_NS=dev-tap

TDP_IMG_LOCATION=$(imgpkg describe -b $(kubectl get -n tap-install $(kubectl get package -n tap-install \
--field-selector spec.refName=tpb.tanzu.vmware.com -o name) -o \
jsonpath="{.spec.template.spec.fetch[0].imgpkgBundle.image}") -o yaml --tty=true | grep -A 1 \
"kbld.carvel.dev/id:[[:blank:]]*[^[:blank:]]*configurator" | grep "image:" | sed 's/[[:blank:]]*image:[[:blank:]]*//g')

TDP_ENCODED_CONFIG=$(base64 -i "./tdp-config.yml")

ytt -f tdp-workload.yml -v tdp_encoded_config="$TDP_ENCODED_CONFIG" -v tdp_img_location="$TDP_IMG_LOCATION" -v dev_ns="$DEV_NS" > $RENDERED_FILES_DIR/tdp-workload.yml

kubectl apply -f $RENDERED_FILES_DIR/tdp-workload.yml