# Helpful Commands for TKG while Airgapped

Login to TKG Supervisor Cluster
```
export KUBECTL_VSPHERE_PASSWORD=password
kubectl vsphere login --server=<vsphere wcp endpoint> --vsphere-username=administrator@vsphere.local --insecure-skip-tls-verify
```

Login to a Workload Cluster
```
kubectl vsphere login --server=10.92.42.137 --tanzu-kubernetes-cluster-name tanzu-kubernetes-cluster-01 --tanzu-kubernetes-cluster-namespace tanzu-ns-1 --vsphere-username administrator@vsphere.local --insecure-skip-tls-verify
```

Install custom CAs into a Supervisor Cluster
```
apiVersion: run.tanzu.vmware.com/v1alpha3
kind: TkgServiceConfiguration
metadata:
  name: tkg-service-configuration
spec:
  defaultCNI: antrea
  trust:
    additionalTrustedCAs:
      - name: first-cert-name
        data: base64 encoded string of PEM CA Cert
      - name: first-cert-name
        data: base64 encoded string of PEM CA Cert
```

Trigger CA rollout from Supervisor Cluster to Workload Clusters
```
# Create patch.yml
spec:
  settings: 
    network:
      trust: null

# Trigger the rollout
kubectl patch tkc my-cluster -n namespace --type merge --patch-file=patch.yml
```
