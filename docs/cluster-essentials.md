# Cluster Essentials Install

This is a quick primer on installing Cluster Essentials in an airgapped environment.

```
# Configure internal CA for the container registry
kubectl create ns kapp-contoller

# Create secret for the CA
kubectl create secret generic kapp-controller-config \
  -n kapp-controller --from-file caCerts=ca.crt

# Upload to the container registry
imgpkg copy --tar cluster-essentials-bundle.tar \
  --to-repo myregistry.com/cluster-essentials \
  --include-non-distributable-layers \
  --registry-ca-cert-path /path/to/ca.crt

# Grab the SHA of the tagged version & install to cluster
export INSTALL_BUNDLE=myregistry.com/cluster-essentials@sha256:<sha>
export INSTALL_REGISTRY_HOSTNAME=myregistry.com
export INSTALL_REGISTRY_USERNAME=user
export INSTALL_REGISTRY_PASSWORD='password'
./install.sh --yes
```