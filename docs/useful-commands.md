# Useful Commands
This is a list of commands I've found helpful to have at the ready when deploying TAP while airgapped.  I am including this in the repo for ease of use when working with this repo in disconnected environments.

### Installation and setup of TAP

Tanzu CLI version and plugin checks
```
tanzu version
tanzu plugin install all --local /path/to/plugins
tanzu plugin list
```

Copy packages from TAR to repo
```
imgpkg copy \
  --tar tap-packages-$TAP_VERSION.tar \
  --to-repo $IMGPKG_REGISTRY_HOSTNAME/tap-packages \
  --include-non-distributable-layers \
  --registry-ca-cert-path $REGISTRY_CA_PATH
```

Trigger Reconciliation on a Package (kick)
```
kctrl package installed kick -i tap -n tap-install -y
```

Get all values for a given package
```
tanzu package available get metadata-store.apps.tanzu.vmware.com/1.5.0 -n tap-install --values-schema
```

### Working with Workloads

Eric's Tanzu App Workload alias
```
alias taw="tanzu app workload"
```

Update a workload label because you forgot to add it
```
tanzu app workload update node-pg --label apps.tanzu.vmware.com/has-tests=true -n dev-tap
```

Create a workload from a local source directory.  Requires a container repo to push the source to
```
tanzu app workload create from-local-path --local-path . -n dev-tap --source-image myrepo.com/tap/supply-chain/from-local-path --type web --label app.kubernetes.io/part-of=tanzu-java-web-app
```

Add label to containers built by TBS
```
# In your workload.yml...
spec:
  build:
    env:
    - name: BP_IMAGE_LABELS
      value: "mylabel:becauseIHaveTo"
```
